***************
Autodoc
***************

.. toctree::
	:maxdepth: 2

	admin.rst
	models.rst
	tests.rst
	views.rst
