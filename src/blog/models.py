from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils import timezone

from autoslug import AutoSlugField
from embed_video.fields import EmbedVideoField
from polymorphic.models import PolymorphicModel
from sortedm2m.fields import SortedManyToManyField

class Image(models.Model):

    creation_date = models.DateTimeField(default=timezone.now)
    image = models.ImageField(upload_to="image_ressource/")
    alt = models.CharField(max_length=100)

    def __str__(self):
        return self.alt

class ReadList(models.Model):

    name = models.CharField(max_length=50, unique=True)
    slug = AutoSlugField(populate_from='name')
    description = models.TextField(max_length=5000)
    abstract = models.TextField(max_length=250)
    creation_date = models.DateTimeField(default=timezone.now)
    edition_date = models.DateTimeField(default=timezone.now)
    publish_date = models.DateTimeField(null=True, blank=True)
    public = models.BooleanField(default=False)
    author = models.CharField(max_length=50, null=True, blank=True)
    thumbnail = models.ForeignKey(Image, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.name

class Tag(models.Model):

    name = models.CharField(max_length=50, unique=True)
    slug = AutoSlugField(populate_from='name')
    description = models.TextField(max_length=5000)

    def __str__(self):
        return self.name

class ArticlePart(PolymorphicModel):

    title = models.CharField(max_length=100, blank=True, null=True)
    level = models.IntegerField(
        default=1,
        validators=[
            MaxValueValidator(3),
            MinValueValidator(1)
        ]
    )

    def __str__(self):
        if self.title:
            return "Part : " + self.title
        else:
            return "Part : No title article part"

class Article(ReadList):

    TECH_LEVEL_LIST = (
        ("1", "Low"),
        ("2", "Medium"),
        ("3", "High"),
    )
    
    readlist = models.ForeignKey(ReadList, on_delete=models.SET_NULL, blank=True, null=True, related_name="child_list")
    tags = models.ManyToManyField(Tag)
    parts = SortedManyToManyField(ArticlePart)
    tech_level = models.CharField(choices=TECH_LEVEL_LIST, max_length=10, default=TECH_LEVEL_LIST[0][0])

class PartText(ArticlePart):

    content = models.TextField(max_length=50000)

    def __str__(self):
        if self.title:
            return "Part : " + self.title
        else:
            return "Part : " + self.content[0:20]

class PartImage(ArticlePart):

    content_before = models.TextField(max_length=50000, blank=True, null=True)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, blank=True, null=True)
    content_after = models.TextField(max_length=50000, blank=True, null=True)

    def __str__(self):
        if self.title:
            return "Part : " + self.title
        else:
            return "Part : " + self.image.alt

class PartTweet(ArticlePart):

    content_before = models.TextField(max_length=50000, blank=True, null=True)
    url = models.URLField(max_length=200)
    content_after = models.TextField(max_length=50000, blank=True, null=True)

    def __str__(self):
        if self.title:
            return "Part : " + self.title
        else:
            return "Part : " + self.url

class PartVideo(ArticlePart):

    content_before = models.TextField(max_length=50000, blank=True, null=True)
    url = EmbedVideoField(max_length=200)
    content_after = models.TextField(max_length=50000, blank=True, null=True)

    def __str__(self):
        if self.title:
            return "Part : " + self.title
        else:
            return "Part : " + self.content[0:20]
