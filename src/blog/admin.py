from django.contrib import admin
from django.contrib.sessions.models import Session
from django.utils.html import format_html

from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin

from blog.models import Image, Tag, Article, ReadList, ArticlePart, PartText, PartImage, PartTweet, PartVideo

admin.site.site_header = 'Vulgarisation digitale'
admin.site.site_title = 'Vulgarisation digitale'
# admin.site.site_url = 'https://vulgarisation-digitale.fr/'
admin.site.index_title = 'Administration'
# admin.empty_value_display = '...'

@admin.register(Session)
class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return obj.get_decoded()
    list_display = ['session_key', '_session_data', 'expire_date']

@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):

    list_display = ['alt', 'creation_date', 'image_tag']

    def image_tag(self, element):
        return format_html('<img src="{}" />'.format(element.image.url))

    image_tag.short_description = 'Image'

@admin.register(Tag)
class TagsAdmin(admin.ModelAdmin):
    
    list_display = ['name', 'slug', 'articles']

    def articles(self, element):
        return Article.objects.filter(tags=element).count()

@admin.register(ReadList)
class ReadListAdmin(admin.ModelAdmin):
    
    list_display = ['name', 'abstract', 'edition_date', 'public', 'author']

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    
    list_display = ['name', 'abstract', 'edition_date', 'public', 'author', 'tag_list']

    def tag_list(self, element):
        return ', '.join([tag.name for tag in element.tags.all()])

@admin.register(ArticlePart)
class ArticlePartAdmin(PolymorphicParentModelAdmin):
    
    list_display = ['title', 'level']
    base_model = ArticlePart
    child_models = (PartText, PartImage, PartTweet, PartVideo)

@admin.register(PartText)
class PartTextAdmin(PolymorphicChildModelAdmin):
    
    base_model = ArticlePart

@admin.register(PartImage)
class PartImageAdmin(PolymorphicChildModelAdmin):
    
    base_model = ArticlePart

@admin.register(PartTweet)
class PartTweetAdmin(PolymorphicChildModelAdmin):
    
    base_model = ArticlePart

@admin.register(PartVideo)
class PartVideoAdmin(PolymorphicChildModelAdmin):

    base_model = ArticlePart