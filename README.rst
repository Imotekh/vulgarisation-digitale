**************************
Vulgarisation Digitale
**************************

Le code de mon blog de vulgarisation sur des sujets en lien avec le numérique.

.. image:: https://gitlab.com/Imotekh/vulgarisation-digitale/badges/master/pipeline.svg
   :target: https://gitlab.com/Imotekh/vulgarisation-digitale/-/commits/master
   :alt: Pipeline Status
.. image:: https://coveralls.io/repos/gitlab/Imotekh/vulgarisation-digitale/badge.svg?branch=HEAD
   :target: https://coveralls.io/gitlab/Imotekh/vulgarisation-digitale?branch=HEAD
.. image:: https://img.shields.io/badge/License-MIT-yellow.svg
   :target: https://opensource.org/licenses/MIT
   :alt: MIT license

Documentation : https://imotekh.gitlab.io/vulgarisation-digitale 